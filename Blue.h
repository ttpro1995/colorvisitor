#pragma once
#include "color.h"
#include "Visitor.h"
class Blue :
	public Color
{
public:
	Blue(void);
	~Blue(void);
	void accept(Visitor& visitor);
};

