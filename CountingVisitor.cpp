#include "CountingVisitor.h"


CountingVisitor::CountingVisitor(void)
{
	num_red=0;
	num_blue=0;
}


CountingVisitor::~CountingVisitor(void)
{
}

void CountingVisitor::visit(Red* a)
{
	num_red++;
}

void CountingVisitor::visit(Blue* a)
{
	num_blue++;
}

void CountingVisitor::Show()
{
	cout<<"Num of red "<<num_red<<endl;
		cout<<"Num of blue "<<num_blue<<endl;


}