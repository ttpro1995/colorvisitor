#pragma once
class Visitor;

class Visitable
{
public:
	virtual void accept(Visitor& visitor)=0;
	Visitable(void);
	~Visitable(void);
};

