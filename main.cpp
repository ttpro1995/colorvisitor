#include <iostream>
using namespace std;
#include "Red.h"
#include "Blue.h"
#include "CountingVisitor.h"
int main()
{
	Color **A;
	A=new Color*[100];
	for (int i=0;i<55;i++)
	{
		A[i]=new Red;
	}
	for (int i=55;i<100;i++)
	{
		A[i]=new Blue;
	}

	CountingVisitor counting;
	for (int i=0;i<100;i++)
	{
	
		A[i]->accept(counting);
	}
	counting.Show();
	return 0;
}