#pragma once
#include "visitor.h"
#include "Blue.h"
#include "Red.h"
class CountingVisitor :
	public Visitor
{
private:
	int num_red;
	int num_blue;
public:
	CountingVisitor(void);
	~CountingVisitor(void);
	void visit(Red*);
	void visit(Blue*);
	void Show();
};

